using UnityEditor;

namespace Creamos.UIEssentials.Editor
{
    public static class EditorUIEssentials
    {
        public static string GetUXMLPath(this EditorWindow win)
        {
            return System.IO.Path.ChangeExtension(AssetDatabase.GetAssetPath(MonoScript.FromScriptableObject(win)), "uxml");
        }

        public static string GetUSSPath(this EditorWindow win)
        {
            return System.IO.Path.ChangeExtension(AssetDatabase.GetAssetPath(MonoScript.FromScriptableObject(win)), "uss");
        }
    }
}
